defined_text = {
	name = GetENL_LONG_TIME
	text = {
		localisation_key = ENL_LONG_TIME_RED
		trigger = {
			NOR = {
				religion = witchcraft
				religion = tui_and_la
				religion = barbarism
				has_game_rule = {
					name = enlightened_restriction
					value = no_restriction
				}
			}
		}
	}
	text = {
		localisation_key = ENL_LONG_TIME_NOT_RED
		trigger = {
			AND = {
				OR = {
					religion = witchcraft
					religion = tui_and_la
					religion = barbarism
				}
				NOT = {
					has_game_rule = {
						name = enlightened_restriction
						value = no_restriction
					}
				}
			}
		}
	}
	text = {
		localisation_key = ENL_LONG_TIME_EQUAL
		trigger = {
			has_game_rule = {
				name = enlightened_restriction
				value = no_restriction
			}
		}
	}
}