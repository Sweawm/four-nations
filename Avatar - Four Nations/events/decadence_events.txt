###################################
#
# Events for Decadence in fire_religions Dynasties
#
# Id 91000 - 91499 reserved
#
###################################

# Currently the Fire religions not using decadence so it is needed to think over, added trigger always no

# Drunkard
character_event = {
	id = 91200
	desc = EVTDESC91200
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		always = no
		uses_decadence = yes
		trait = drunkard
		NOT = { has_character_flag = decadence_drunkard }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91200
		decadence = 2
		set_character_flag = decadence_drunkard
	}
}

# Homosexual
character_event = {
	id = 91201
	desc = EVTDESC91201
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		always = no
		uses_decadence = yes
		trait = homosexual
		NOT = { has_character_flag = decadence_homosexual }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91201
		decadence = 2
		set_character_flag = decadence_homosexual
	}
}

# Hedonist
character_event = {
	id = 91202
	desc = EVTDESC91202
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = hedonist
		NOT = { has_character_flag = decadence_hedonist }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91202
		decadence = 2
		set_character_flag = decadence_hedonist
	}
}

# Impaler
character_event = {
	id = 91203
	desc = EVTDESC91203
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = impaler
		NOT = { has_character_flag = decadence_impaler }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91203
		decadence = 2
		set_character_flag = decadence_impaler
	}
}

# Lustful
character_event = {
	id = 91204
	desc = EVTDESC91204
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = lustful
		NOT = { trait = homosexual }
		NOT = { has_character_flag = decadence_lustful }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91204
		decadence = 2
		set_character_flag = decadence_lustful
	}
}

# Gluttonous
character_event = {
	id = 91205
	desc = EVTDESC91205
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = gluttonous
		NOT = { has_character_flag = decadence_gluttonous }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91205
		decadence = 2
		set_character_flag = decadence_gluttonous
	}
}

# Greedy
character_event = {
	id = 91206
	desc = EVTDESC91206
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = greedy
		NOT = { has_character_flag = decadence_greedy }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91206
		decadence = 2
		set_character_flag = decadence_greedy
	}
}

# Slothful
character_event = {
	id = 91207
	desc = EVTDESC91207
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = slothful
		NOT = { has_character_flag = decadence_slothful }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91207
		decadence = 2
		set_character_flag = decadence_slothful
	}
}

# Envious
character_event = {
	id = 91208
	desc = EVTDESC91208
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = envious
		NOT = { has_character_flag = decadence_envious }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91208
		decadence = 2
		set_character_flag = decadence_envious
	}
}

# Wroth
character_event = {
	id = 91209
	desc = EVTDESC91209
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = wroth
		NOT = { has_character_flag = decadence_wroth }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91209
		decadence = 2
		set_character_flag = decadence_wroth
	}
}

# Proud
character_event = {
	id = 91210
	desc = EVTDESC91210
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = proud
		NOT = { has_character_flag = decadence_proud }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91210
		decadence = 2
		set_character_flag = decadence_proud
	}
}

# Deceitful
character_event = {
	id = 91211
	desc = EVTDESC91211
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = deceitful
		NOT = { has_character_flag = decadence_deceitful }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91211
		decadence = 2
		set_character_flag = decadence_deceitful
	}
}

# Craven
character_event = {
	id = 91212
	desc = EVTDESC91212
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = craven
		NOT = { has_character_flag = decadence_craven }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91212
		decadence = 2
		set_character_flag = decadence_craven
	}
}

# Content
character_event = {
	id = 91213
	desc = EVTDESC91213
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = content
		NOT = { has_character_flag = decadence_content }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91213
		decadence = 2
		set_character_flag = decadence_content
	}
}

# Arbitrary
character_event = {
	id = 91214
	desc = EVTDESC91214
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = arbitrary
		NOT = { has_character_flag = decadence_arbitrary }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91214
		decadence = 2
		set_character_flag = decadence_arbitrary
	}
}

# Cynical
character_event = {
	id = 91215
	desc = EVTDESC91215
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = cynical
		NOT = { has_character_flag = decadence_cynical }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91215
		decadence = 2
		set_character_flag = decadence_cynical
	}
}

# Paranoid
character_event = {
	id = 91216
	desc = EVTDESC91216
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = paranoid
		NOT = { has_character_flag = decadence_paranoid }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91216
		decadence = 2
		set_character_flag = decadence_paranoid
	}
}

# Cruel
character_event = {
	id = 91217
	desc = EVTDESC91217
	picture = GFX_evt_night_firenation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = cruel
		NOT = { has_character_flag = decadence_cruel }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91217
		decadence = 2
		set_character_flag = decadence_cruel
	}
}

### Lowers Decadence

# Genius
character_event = {
	id = 91218
	desc = EVTDESC91218
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		is_smart_trigger = yes
		NOT = { has_character_flag = decadence_genius }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91218
		decadence = -2
		set_character_flag = decadence_genius
	}
}

# Celibate
character_event = {
	id = 91219
	desc = EVTDESC91219
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = celibate
		NOT = { has_character_flag = decadence_celibate }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91219
		decadence = -2
		set_character_flag = decadence_celibate
	}
}

# Scholar
character_event = {
	id = 91220
	desc = EVTDESC91220
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = scholar
		NOT = { has_character_flag = decadence_scholar }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91220
		decadence = -2
		set_character_flag = decadence_scholar
	}
}

# Chaste
character_event = {
	id = 91221
	desc = EVTDESC91221
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = chaste
		NOT = { has_character_flag = decadence_chaste }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91221
		decadence = -2
		set_character_flag = decadence_chaste
	}
}

# Temperate
character_event = {
	id = 91222
	desc = EVTDESC91222
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = temperate
		NOT = { has_character_flag = decadence_temperate }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91222
		decadence = -2
		set_character_flag = decadence_temperate
	}
}

# Charitable
character_event = {
	id = 91223
	desc = EVTDESC91223
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = charitable
		NOT = { has_character_flag = decadence_charitable }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91223
		decadence = -2
		set_character_flag = decadence_charitable
	}
}

# Diligent
character_event = {
	id = 91224
	desc = EVTDESC91224
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = diligent
		NOT = { has_character_flag = decadence_diligent }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91224
		decadence = -2
		set_character_flag = decadence_diligent
	}
}

# Kind
character_event = {
	id = 91225
	desc = EVTDESC91225
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = kind
		NOT = { has_character_flag = decadence_kind }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91225
		decadence = -2
		set_character_flag = decadence_kind
	}
}

# Patient
character_event = {
	id = 91226
	desc = EVTDESC91226
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = patient
		NOT = { has_character_flag = decadence_patient }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91226
		decadence = -2
		set_character_flag = decadence_patient
	}
}

# Humble
character_event = {
	id = 91227
	desc = EVTDESC91227
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = humble
		NOT = { has_character_flag = decadence_humble }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91227
		decadence = -2
		set_character_flag = decadence_humble
	}
}

# Honest
character_event = {
	id = 91228
	desc = EVTDESC91228
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = honest
		NOT = { has_character_flag = decadence_honest }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91228
		decadence = -2
		set_character_flag = decadence_honest
	}
}

# Brave
character_event = {
	id = 91229
	desc = EVTDESC91229
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = brave
		NOT = { has_character_flag = decadence_brave }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91229
		decadence = -2
		set_character_flag = decadence_brave
	}
}

# Ambitious
character_event = {
	id = 91230
	desc = EVTDESC91230
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = ambitious
		NOT = { has_character_flag = decadence_ambitious }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91230
		decadence = -2
		set_character_flag = decadence_ambitious
	}
}

# Just
character_event = {
	id = 91231
	desc = EVTDESC91231
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = just
		NOT = { has_character_flag = decadence_just }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91231
		decadence = -2
		set_character_flag = decadence_just
	}
}

# Zealous
character_event = {
	id = 91232
	desc = EVTDESC91232
	picture = GFX_evt_fire_nation
	
	ai = no
	min_age = 16
	capable_only = yes
	prisoner = no
	only_rulers = yes
	religion_group = fire_religions
	
	trigger = {
		always = no
		uses_decadence = yes
		trait = zealous
		NOT = { has_character_flag = decadence_zealous }
	}
	
	mean_time_to_happen = {
		months = 1200
	}
	
	option = {
		name = EVTOPTA91232
		decadence = -2
		set_character_flag = decadence_zealous
	}
}

narrative_event = {
	id = 91241
	title = EVTTITLE91241
	desc = EVTDESC91241
	picture = GFX_evt_fire_nation
	
	is_triggered_only = yes
	
	option = {
		name = EVTOPTA91241
	}
}

# IMPORTANT EVENT
# Fire gains the Dishonourable Trait

character_event = {
	id = 91300
	desc = EVTDESC91300
	picture = GFX_evt_night_firenation
	
	min_age = 16
	capable_only = yes
	prisoner = no
	religion_group = fire_religions
	
	trigger = {
		always = no
		num_of_dynasty_members = 6 # Does not trigger for tiny dynasties
		uses_decadence = yes
		NOT = { trait = decadent }
		NOT = { trait = zealous }
		lower_tier_than = KING
		NOT = { has_character_modifier = decadent_timer }
		biggest_realm_size_relative = {
			NOT = { character = PREV }
			realm_size = 5
		}
	}
	
	mean_time_to_happen = {
		months = 90
		
		modifier = {
			factor = 0.5
			trait = indulgent_wastrel
		}
		modifier = {
			factor = 0.75
			trait = naive_appeaser
		}
		modifier = {
			factor = 0.75
			trait = amateurish_plotter
		}
		modifier = {
			factor = 0.25
			trait = hedonist
		}
		modifier = {
			factor = 0.5
			trait = homosexual
		}
		modifier = {
			factor = 0.5
			trait = gluttonous
		}
		modifier = {
			factor = 0.5
			trait = slothful
		}
		modifier = {
			factor = 0.75
			trait = cynical
		}
		modifier = {
			factor = 0.75
			trait = lustful
		}
		modifier = {
			factor = 0.75
			trait = arbitrary
		}
		modifier = {
			factor = 3.0
			trait = chaste
		}
		modifier = {
			factor = 3.0
			trait = just
		}
		modifier = {
			factor = 10.0
			trait = diligent
		}
		modifier = {
			factor = 10.0
			trait = temperate
		}
		modifier = {
			factor = 2.0
			trait = detached_priest
		}
		modifier = {
			factor = 4.0
			trait = martial_cleric
		}
		modifier = {
			factor = 8.0
			trait = scholarly_theologian
		}
		modifier = {
			factor = 10.0
			trait = mastermind_theologian
		}
		modifier = {
			factor = 2.5
			tier = BARON
		}
		modifier = {
			factor = 10.0
			tier = COUNT
		}
		modifier = {
			factor = 20.0
			tier = DUKE
		}
		modifier = {
			factor = 0.5
			highest_ranked_relative = {
				tier = KING
			}
		}
		modifier = {
			factor = 0.5
			highest_ranked_relative = {
				tier = EMPEROR
			}
		}
		modifier = {
			factor = 0.5
			biggest_realm_size_relative = {
				realm_size = 150
			}
		}
		modifier = {
			factor = 0.5
			biggest_realm_size_relative = {
				realm_size = 250
			}
		}
	}
	
	option = {
		name = EVTOPTA91300
		add_trait = decadent
		
		hidden_tooltip = {
			any_dynasty_member = {
				limit = {
					ai = no
				}
				character_event = {
					id = 91370
					days = 1
				}
			}
		}
	}
}

# Fire ruler tries to convince a dynasty member to lose the 'Decadent' Trait
# Fired from decision "convince_to_straighten_up"
character_event = {
	id = 91350
	desc = EVTDESC91350
	picture = GFX_evt_night_firenation
	
	is_triggered_only = yes
	
	option = {
		name = EVTOPTA91350 # Refuse
		ai_chance = {
			factor = 10
			modifier = {
				factor = 2
				NOT = {
					opinion = {
						who = FROMFROM
						value = 0
					}
				}
			}
			modifier = {
				factor = 2
				NOT = {
					opinion = {
						who = FROMFROM
						value = -25
					}
				}
			}
			modifier = {
				factor = 4
				NOT = {
					opinion = {
						who = FROMFROM
						value = -50
					}
				}
			}
			modifier = {
				factor = 4
				trait = hedonist
			}
			modifier = {
				factor = 2
				trait = gluttonous
			}
			modifier = {
				factor = 2
				trait = arbitrary
			}
			modifier = {
				factor = 2
				trait = drunkard
			}
			modifier = {
				factor = 1.5
				trait = slothful
			}
			modifier = {
				factor = 1.5
				diplomacy = 12
			}
			modifier = {
				factor = 1.5
				learning = 12
			}
			modifier = {
				factor = 1.5
				intrigue = 12
			}
		}
		FROMFROM = {
			character_event = {
				id = 91351
				days = 3
				tooltip = EVTTOOLTIP91351
			}
		}
		character_event = {	id = 91371 days = 3650 }
	}
	
	option = {
		name = EVTOPTB91350 # Accept
		ai_chance = {
			factor = 5
			modifier = {
				factor = 3
				opinion = {
					who = FROMFROM
					value = 25
				}
			}
			modifier = {
				factor = 3
				opinion = {
					who = FROMFROM
					value = 50
				}
			}
			modifier = {
				factor = 3
				opinion = {
					who = FROMFROM
					value = 75
				}
			}
		}
		FROMFROM = {
			character_event = {
				id = 91352
				days = 3
				tooltip = EVTTOOLTIP91352
			}
		}
		add_character_modifier = {
			modifier = decadent_timer
			years = 5
			hidden = yes
		}
		remove_trait = decadent
		clr_character_flag = negotiated_to_drop_decadence
	}
}

# Player rulers of the dynasty are notified when someone goes decadent
character_event = {
	id = 91370
	desc = EVTDESC91370
	picture = GFX_evt_jugglers
	
	notification = yes
	
	is_triggered_only = yes
	
	option = {
		name = EVTOPTA91370
	}
}

# Resets flag negotiated_to_drop_decadence
character_event = {
	id = 91371
	
	hide_window = yes
	
	is_triggered_only = yes
	immediate = {
		clr_character_flag = negotiated_to_drop_decadence
	}
}
